<?

/**
 * Plugin Name: Mostrar parcela de 18x
 * Description: Added the price with 18 installments with interest calculated using the factor of pagseguro.
 * Author: claudiosanches, Tiarê Balbi, Marcos Freitas
 * Version: 1.0
 * License: GPLv2 or later
 */


class Pagseguro {
        
        /**
         * Fator para calculo de juros em parcela
         * @var array
         */
        /*
           fator de juros verificado em  10/09/2013
        1       R$ 1.000,00     R$ 1.000,00     1.00000
        2       R$ 522,55       R$ 1.045,10     0.52255
        3       R$ 353,47       R$ 1.060,40     0.35347
        4       R$ 268,98       R$ 1.075,90     0.26898
        5       R$ 218,30       R$ 1.091,50     0.21830
        6       R$ 184,53       R$ 1.107,20     0.18453
        7       R$ 160,44       R$ 1.123,10     0.16044
        8       R$ 142,40       R$ 1.139,20     0.14240
        9       R$ 128,38       R$ 1.155,40     0.12838
        10      R$ 117,17       R$ 1.171,70     0.11717
        11      R$ 108,02       R$ 1.188,20     0.10802
        12      R$ 100,40       R$ 1.204,80     0.10040
        13      R$ 93,97        R$ 1.221,60     0.09397
        14      R$ 88,46        R$ 1.238,50     0.08846
        15      R$ 83,71        R$ 1.255,60     0.08371
        16      R$ 79,55        R$ 1.272,80     0.07955
        17      R$ 75,89        R$ 1.290,20     0.07589
        18      R$ 72,65        R$ 1.307,70     0.07265

        */
        var $fator = array(
                       '1'=>'1',
                        '2'=>'0.52255',
                        '3'=>'0.35347',
                        '4'=>'0.26898',
                        '5'=>'0.21830',
                        '6'=>'0.18453',
                        '7'=>'0.16044',
                        '8'=>'0.14240',
                        '9'=>'0.12838',
                        '10'=>'0.11717',
                        '11'=>'0.10802',
                        '12'=>'0.10040',
                        '13'=>'0.09397',
                        '14'=>'0.08846',
                        '15'=>'0.08371',
                        '16'=>'0.07955',
                        '17'=>'0.07589',
                        '18'=>'0.07265'
        );
        
        /**
         * Bandeiras das empresas de cartão de credito e o numero maximo de parcelas 
         * @var array
         */
        var $bandeiras = array(
                        '1'=>array('nome'=>'Visa','parcelas'=>'12'),
                        '2'=>array('nome'=>'MasterCard','parcelas'=>'12'),
                        '3'=>array('nome'=>'Diners','parcelas'=>'12'),
                        '4'=>array('nome'=>'American Express','parcelas'=>'15'),
                        '5'=>array('nome'=>'Hipercard','parcelas'=>'12'),
                        '6'=>array('nome'=>'Aura','parcelas'=>'18'),
                        '7'=>array('nome'=>'Elo','parcelas'=>'12'),
                        '8'=>array('nome'=>'PLENOcard','parcelas'=>'3'),
                        '9'=>array('nome'=>'Personalcard','parcelas'=>'3'),
                        '10'=>array('nome'=>'Brasilcard','parcelas'=>'3'),
                        '11'=>array('nome'=>'FORTBRASIL','parcelas'=>'12'),
                        '12'=>array('nome'=>'CARDBAN','parcelas'=>'12'),
                        '13'=>array('nome'=>'VALEcard','parcelas'=>'3'),
                        '14'=>array('nome'=>'Cabal','parcelas'=>'12'),
                        '15'=>array('nome'=>'Mais!','parcelas'=>'10'),
                        '16'=>array('nome'=>'Avista','parcelas'=>'6'),
                        '17'=>array('nome'=>'Grandcard','parcelas'=>'6')     
        );

        var $parcela_minima = 5.00;
        
        /**
         * Converte o valor para formato numerico valido
         * @param string $valor
         */
        public function formatNumber($valor){
                
                $preco = (!empty($valor) ? $valor : "0.00");            
                return number_format($preco, 2, ',', '');
                 
        }
        
        /**
         * Converte o valor para um valor inteiro sem casa decimal
         * @param int $valor
         */
        public function formatInteiro($valor){
                $preco = (!empty($valor) ? $valor : "0.00");
                return number_format($preco, 0, ',', '');
                
        }
        
        /**
         * Realiza o calculo e cria uma array retornando o valor da parcela, total de parcela, total pago respectiva parcela.
         * @param int $valor
         * @param int $cartao
         */
        public function parcelamento($valor,$cartao){
                
                $valor = str_replace(",", ".", $valor);
                
                $retorno = array();
                
                //$retorno['cartao'] = $this->bandeiras[$cartao]['nome'];
                $num_parcela = $this->bandeiras[$cartao]['parcelas'];
                
                for($i=1; $i<=$num_parcela; $i++){

                        $valor_parcela = $this->formatNumber($valor*$this->fator[$i]);
                        
                        //se a parcela passar a ficar mais baixa que a parcela minima o loop é encerrado e a parcela anterior fica salva
                        if( $i != 1 && $this->formatInteiro($this->parcela_minima) > $this->formatInteiro($valor_parcela)){
                                break;
                        }
                                                
                        $retorno['valores'][$i] = array(
                                        'prestacao'=>$i,
                                        'valor_parcela'=>$valor_parcela,
                                        'total_pago'=>$this->formatNumber($valor_parcela*$i)
                        );
                        
                }

                return $retorno;
        }

}

/**
 * Calculates the price in 3 installments without interest.
 *
 * @return string Price in 3 installments.
 */
function cs_product_parceled() {
    $product = get_product();

    if ( $product->get_price_including_tax() ) {

        $pg = new Pagseguro();

        # Primeiro campo é o valor que você deseja simular
        # Segundo campo é a bandeira do cartão, lista de bandeiras abaixo:
        /*
          
        1: Visa
        2: MasterCard
        3: Diners
        4: American Express
        5: Hipercard
        6: Aura
        7: Elo
        8: PLENOcard
        9: Personalcard
        10: Brasilcard
        11: FORTBRASIL
        12: CARDBAN
        13: VALEcard
        14: Cabal
        15: Mais!
        16: Avista
        17: Grandcard

        */

        # $valor['cartao'] : nome da bandeira
        # $valor['prestacao'] : número da parcela
        # $valor['valor'] : valor calculado da parcela
        # $retorno['valores'][$i] : prestacao valor total_pago
        # A função Parcelamento retornará um array()

        # obtendo o valor total da compra ou do produto pela função do woocommerce e enviando ao método de parcelamento da classe pagseguro acima
        # foi definido o valor da bandeira 6 (aura) que tem o maior número de parcelas (18)

        $retorno = $pg->parcelamento($product->get_price_including_tax(),'6');
        //var_dump($retorno);
        $tamanho_array = sizeof($retorno['valores']);

        #vai percorrer apenas o último item do array
        #criar variáveis: prestacao, valor_parcela, total_pago
        foreach ($retorno['valores'][$tamanho_array] as $key => $value) {
            
            $$key = $value;

        }
        //retornando o valor da parcela e formatando com a função woocommerce_price
        //$value = woocommerce_price($valor_parcela);
        echo 'at&eacute; '.$prestacao.'x de ' .woocommerce_price($valor_parcela);
    }
}

/**
 * Displays the Installments on product loop.
 * 
 * @return string Price in 3 installments.
 */
function cs_product_parceled_loop() {
    echo '<p class="parceled-price">' . cs_product_parceled() . '</p>';
}

/**
 * Displays the Installments on the product page.
 *
 * @return string Price in 3 installments.
 */
function cs_product_parceled_single() {
    $product = get_product();
?>
    <div class="offers-principal" itemprop="offers" itemscope itemtype="http://schema.org/Offer">

        <p style="margin: 0;" itemprop="price" class="price">
            <?php echo woocommerce_price( $product->get_price() ); ?>
        </p>
        <p>
            <span class="parceled-price"><?php cs_product_parceled(); ?></span>
        </p>

        <meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
        <link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
<?php
        echo '<div class="service-post-short-description">';
        //var_dump($product);
        the_excerpt();
        echo '</div>';
                                
?>


    </div>
<?php
}

   add_action( 'woocommerce_after_shop_loop_item_title', 'cs_product_parceled_loop', 20 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 20 );

   add_action( 'woocommerce_single_product_summary', 'cs_product_parceled_single', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

//remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
/*
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
*/
?>